(function() {

    var promise_ring = require('../promise_ring.js');


    exports.fulfilled = function(value) {
        var promise = new promise_ring.Promise();
        promise.fulfill(value);
        return promise;
    };


    exports.rejected = function(reason) {
        var promise = new promise_ring.Promise();
        promise.reject(reason);
        return promise;
    };


    exports.pending = function() {

        var promise = new promise_ring.Promise();

        return {
            promise: promise,
            fulfill: function(value) {
                promise.fulfill(value);
            },
            reject: function(reason) {
                promise.reject(reason);
            }
        };
    };

}).call(this);
