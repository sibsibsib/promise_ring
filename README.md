Promise Ring
============

A light implementation of Promises/A+. Compliant with promises-aplus-tests.


Usage
-----

### constructor


#### Promise()

    var promise = new PromiseRing.Promise();


### callbacks


#### then()

    promise.then(onFulfilledCallback, onRejectedCallback);


This method is chainable, per the A+ spec.


#### always()

    promise.always(onCompleteCallback)


The always callback will execute no matter which state the promise ends up in.


### resolution

#### fulfill()

    promise.fulfill(value)


Fullfills the promise with the passed in value. This will set the promise
to a fulfilled state and trigger any success callbacks.


#### reject()

    promise.reject(reason)

Rejects the promise with the passed in value. This will set the promise to
a rejected state and trigger any failure callbacks.


### aliases

The following is a list of convenience methods that map to others listed above:

* ``resolve(value)`` -> ``fullfill(value)``
* ``success(callback)`` ->  ``then(callback, undefined)``
* ``fail(callback)`` -> ``then(undefined, callback)``


### combination

#### PromiseRing.all()

    var new_promise = PromiseRing.all([/* ... array of promises... */]);

Returns a new promise that is resolved only when *all* of the child promises
are resolved. The value of the resolution is an array of all the resolved values,
in the same order the promises were originally passed in.


#### PromiseRing.any()

    var new_promise = PromiseRing.any([/* ...array of promises... */]);

Returns a new Promise that is resolved when *any* of the child promises
is resolved. The value of the resolution is that of the child promise which
was resolved.


Development
-----------

    # install dependencies for testing
    $ npm install

    # run tests
    $ npm test


Currently the only tests are the promises-aplus-tests suite. Other tests are TODO.

