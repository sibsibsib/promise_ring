
(function(){
    'use strict';

    var global = this;

    var PromiseRing = global.PromiseRing || {};


    var PromiseState = {
        pending: 'pending',
        fulfilled: 'fulfilled',
        rejected: 'rejected'
    };


    var Promise = PromiseRing.Promise = function() {
        this._state = PromiseState.pending;
        this._value = undefined;
        this._on_fulfilled = [];
        this._on_rejected = [];

        this.value = undefined;
        this.reason = undefined;
    };


    // --- public methods --- //


    Promise.prototype.fulfill = function(value) {
        this._setState(PromiseState.fulfilled, value);
    };


    Promise.prototype.reject = function(reason) {
        this._setState(PromiseState.rejected, reason);
    };


    Promise.prototype.then = function(onFulfilled, onRejected) {
        var child_promise = new Promise();

        this._on_fulfilled.push(this._callBackWrapper.bind(this, onFulfilled, child_promise));
        this._on_rejected.push(this._callBackWrapper.bind(this, onRejected, child_promise));

        delay(this._trigger.bind(this));
        return child_promise;
    };

    // TODO:
    // .done(): explode unhandled rejections

    Promise.prototype.always = function(onComplete) {
        return this.then(onComplete, onComplete);
    };

    // --- aliases --- //


    Promise.prototype.resolve = Promise.prototype.fulfill;


    Promise.prototype.success = function(callback) {
        return this.then(callback);
    };


    Promise.prototype.fail = function(callback) {
        return this.then(undefined, callback);
    };


    // --- private --- //


    Promise.prototype._setState = function(state, value) {
        if(this._state !== PromiseState.pending) {
            return;
        }

        this._state = state;
        this.value = value;

        this._trigger();
    };


    Promise.prototype._trigger = function() {
        var callback_name = '_on_' + this._state;
        var callbacks = this[callback_name];

        if(callbacks === undefined) {
            // i.e. for pending state which has no callback
            return;
        }

        for(var i = 0; i < callbacks.length; i++){
            callbacks[i].call(this, this.value);
        }

        // clear out callbacks to prevent mem leaks and/or duplicate calls
        this[callback_name] = [];

    };


    Promise.prototype._callBackWrapper = function(callback, child_promise, value) {
        var res;

        if(isfn(callback)) {
            try{
                res = callback(value);
            }
            catch(e) {
                child_promise.reject(e);
            }
        }
        else {
            if(this._state === PromiseState.rejected) {
                child_promise.reject(value);
            }

            res = value;
        }

        if(!ispromise(res)) {
            child_promise.fulfill(res);
        }
        else {
            res.then(
                function(v) {
                    child_promise.fulfill(v);
                },
                function(r) {
                    child_promise.reject(r);
                }
            );
        }
    };


    // ===== utils ===== //


    PromiseRing.all = function(promises) {
        // returns a new promise resolved with an array of all the values
        // if any are rejected, the whole thing will fail, the reason will
        // be the first promise that failed.
        var new_promise = new Promise();

        var remaining = promises.length;
        var values = new Array(remaining);

        var fulfilled = function(index, value) {
            values[index] = value;
            if(--remaining <= 0) {
                new_promise.fulfill(values);
            }
        };

        var rejected = function(reason) {
            new_promise.reject(reason);
        };

        for(var i = 0; i < promises.length; i++) {
            promises[i].then(fulfilled.bind(new_promise, i), rejected);
        }

        return new_promise;
    };

    PromiseRing.any = function(promises) {
        // returns a new promise resolved as soon as any one of the child
        // promises is resolved. value is the resolved value.
        var new_promise = new Promise();

        var fulfilled = function(value) {
            new_promise.fulfill(value);
        };

        var rejected = function(reason) {
            new_promise.reject(reason);
        };

        for(var i = 0; i < promises.length; i++) {
            promises[i].then(fulfilled, rejected);
        }

        return new_promise;
    };


    var isfn = PromiseRing.isfn = function(fn) {
        return typeof fn === 'function';
    };


    var ispromise = PromiseRing.ispromise = function(obj) {
        return obj instanceof Promise || (obj && isfn(obj.then));
    };


    var _task_queue = [];
    var _task_msg = '__promise_ring__.task';


    var __handleTaskMessage = function(event) {
        if (event.source === global && event.data === _task_msg) {
            event.stopPropagation();
            if (_task_queue.length > 0) {
                var callback = _task_queue.shift();
                callback();
            }
        }
    };


    var __selectDelayFn = function() {
        // HAX: because not every browser supports proper yield/delay func

        if(global.process && isfn(global.process.nextTick)) {
            // NodeJS / test env
            return global.process.nextTick;
        }

        if(isfn(global.setImmediate)) {
            // IE
            return global.setImmediate;
        }

        // Webkit/FFX
        global.addEventListener("message", __handleTaskMessage, true);

        return function(callback) {
            _task_queue.push(callback);
            global.postMessage(_task_msg, "*");
        };
    };


    var delay = PromiseRing.delay = __selectDelayFn();


    // NodeJS module support
    if(typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
        module.exports = PromiseRing;
    }
    else {
        global.PromiseRing = PromiseRing;
    }


}).call(typeof global === 'undefined'? this : global);

